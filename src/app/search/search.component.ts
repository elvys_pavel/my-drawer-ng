import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, View } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";


@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    // resultados: Array<string> = [];
    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    constructor(public noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }
    ngOnInit(): void {
        this.noticias.agregar("Brayan!");
        this.noticias.agregar("Maria!");
        this.noticias.agregar("Mathews!");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
        console.dir(x);
    }
    buscarAhora(s: string) {
        this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }));
    }

    // onPull(e) {
    //     console.log(e);
    //     const pullRefresh = e.object;
    //     setTimeout(() => {
    //         this.resultados.push("xxxxxxx");
    //         pullRefresh.refreshing = false;
    //         }, 2000);
    //     }
}

